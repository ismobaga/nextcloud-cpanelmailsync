/*
 * settings.js
 * Copyright 2020 eth0 <ethernet.zero@gmail.com>
 * 
 * This work is free. You can redistribute it and/or modify it under the terms of
 * the Mozilla Public License 2.0. See the COPYING file for more details.
 */
function setCPanelConfigValue(setting, value) {
	if (setting != 'cpanelApiKey' || (setting == 'cpanelApiKey' && value != '********')) {
		OC.msg.startSaving('#cpanel-indicator');
		OC.AppConfig.setValue('cpanelmailsync', setting, value);
		OC.msg.finishedSaving('#cpanel-indicator', {status: 'success', data: {message: t('cpanelmailsync', 'Saved')}});
	}
}

$(function() {
	'use strict';

	$('#cpanel input, #cpanel textarea').each(function(e) {
		var el = $(this);
		if (!el.val()) {
			el.val(el.attr('data-default'));
		}
	});

	$('#cpanel input, #cpanel textarea').change(function(e) {
		var el = $(this);
		$.when(el.focusout()).then(function() {
			var key = $(this).attr('name');
			setCPanelConfigValue(key, $(this).val());
		});
		if (e.keyCode === 13) {
			var key = $(this).attr('name');
			setCPanelConfigValue(key, $(this).val());
		}
	});
});
