<?php

/**
 * Provides actions to operate on cPanel mail accounts.
 *
 * @package CPanelService
 * @author eth0 <ethernet.zero@gmail.com>
 * @version 0.1
 * @copyright (C) 2020 eth0 <ethernet.zero@gmail.com>
 * @license MPLv2 (https://spdx.org/licenses/MPL-2.0.html)
 */

declare(strict_types=1);

namespace OCA\CPanelMailSync\Service;

use OCP\IConfig;
use OCP\ILogger;

class CPanelService {
	/** @var string */
	private $appName;

	/** @var IConfig */
	private $config;

	/** @var ILogger */
	private $logger;

	/** @var array */
	private $httpContext;

	/** @var string */
	private $cpanelUrlTemplate = 'https://%s:2083/execute/Email/%s_pop?%s';

	public function __construct($AppName, IConfig $config, ILogger $logger) {
		$this->appName = $AppName;
		$this->config = $config;
		$this->logger = $logger;

		/**
		 * Authorization: cpanel ${userName}:${apiToken}
		 */
		$cpanelUser = $config->getAppValue($this->appName, 'cpanelUser');
		$apiToken = $config->getAppValue($this->appName, 'cpanelApiToken');
		$headers = [
			'Content-Type: application/x-www-form-urlencoded',
			"Authorization: cpanel $cpanelUser:$apiToken",
		];

		$this->httpContext = [
			'method' => 'POST',
			'header' => $headers,
		];
	}

	private function _config(string $key): string {
		return $this->config->getAppValue($this->appName, $key);
	}

	private function cpanelEmail(string $action, array $payload): bool {
		$context = stream_context_create(['http' => $this->httpContext]);

		$body = file_get_contents(sprintf(
			$this->cpanelUrlTemplate,
			$this->_config('cpanelHost'),
			$action,
			http_build_query($payload)
		), false, $context);

		$json = json_decode($body, true);

		if (!is_null($json['messages'])) {
			foreach ($json['messages'] as $message) {
				if (!empty($message)) {
					$this->logger->info($message);
				}
			}
		}

		if (!is_null($json['warnings'])) {
			foreach ($json['warnings'] as $message) {
				if (!empty($message)) {
					$this->logger->warning($message);
				}
			}
		}

		if (!is_null($json['errors'])) {
			foreach ($json['errors'] as $message) {
				if (!empty($message)) {
					$this->logger->error($message);
				}
			}
		}

		return $json['status'] == 1;
	}

	/**
	 * New:
	 * https://${domain}:2083/execute/Email/add_pop
	 * email=${userName}
	 * domain=${domain}
	 * password=${password}
	 * quota=${quota}
	 * send_welcome_email=${sendWelcomeEmail}
	 */
	public function createMail($userName, $password): void {
		$this->cpanelEmail('add', [
			'email' => $userName,
			'domain' => $this->_config('cpanelDomain'),
			'password' => $password,
			'quota' => $this->_config('cpanelQuota'),
			'send_welcome_email' => 0,
		]);
	}

	/**
	 * Update password:
	 * https://${domain}:2083/execute/Email/passwd_pop
	 * email=${userName}
	 * domain=${domain}
	 * password=${password}
	 */
	public function updateMail($userName, $password): void {
		$this->cpanelEmail('passwd', [
			'email' => $userName,
			'domain' => $this->_config('cpanelDomain'),
			'password' => $password,
		]);
	}

	/**
	 * Delete:
	 * https://${domain}:2083/execute/Email/delete_pop
	 * email=${userName}
	 * domain=${domain}
	 */
	public function deleteMail($userName): void {
		$this->cpanelEmail('delete', [
			'email' => $userName,
			'domain' => $this->_config('cpanelDomain'),
		]);
	}
}
