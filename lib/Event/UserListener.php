<?php

/**
 * This listener responds to user creations, deletions and password updates.
 *
 * @package UserListener
 * @author eth0 <ethernet.zero@gmail.com>
 * @version 0.1
 * @copyright (C) 2020 eth0 <ethernet.zero@gmail.com>
 * @license MPLv2 (https://spdx.org/licenses/MPL-2.0.html)
 */

declare(strict_types=1);

namespace OCA\CPanelMailSync\Event;

use OCP\EventDispatcher\Event;
use OCP\EventDispatcher\IEventListener;
use OCP\User\Events\UserCreatedEvent;
use OCP\User\Events\PasswordUpdatedEvent;
use OCP\User\Events\UserDeletedEvent;
use OCA\CPanelMailSync\Service\CPanelService;

class UserListener implements IEventListener {
	/** @var CPanelService */
	private $service;

	public function __construct(CPanelService $service) {
		$this->service = $service;
	}

	public function handle(Event $event): void {
		if ($event instanceof UserCreatedEvent) {
			$this->service->createMail($event->getUid(), $event->getPassword());
		} elseif ($event instanceof PasswordUpdatedEvent) {
			$this->service->updateMail($event->getUser()->getUID(), $event->getPassword());
		} elseif ($event instanceof UserDeletedEvent) {
			$this->service->deleteMail($event->getUser()->getUID());
		}
	}
}
