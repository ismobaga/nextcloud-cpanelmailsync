<?php

/**
 * Settings class.
 *
 * @package Settings
 * @author eth0 <ethernet.zero@gmail.com>
 * @version 0.1
 * @copyright (C) 2020 eth0 <ethernet.zero@gmail.com>
 * @license MPLv2 (https://spdx.org/licenses/MPL-2.0.html)
 */

declare(strict_types=1);

namespace OCA\CPanelMailSync\Settings;

use OCP\AppFramework\Http\TemplateResponse;
use OCP\IConfig;
use OCP\Settings\ISettings;

class AdminSettings implements ISettings {

	/** @var string */
	private $appName;

	/** @var IConfig */
	private $config;

	/** @var array */
	private $defaultSettings = [
		'cpanelHost' => 'example.com',
		'cpanelUser' => 'myuser',
		'cpanelDomain' => 'example.com',
		'cpanelApiToken' => 'Y0UR-4P1-T0K3N',
		'cpanelQuota' => 1024,
	];

	public function __construct($AppName, IConfig $config) {
		$this->appName = $AppName;
		$this->config = $config;
	}

	public function getForm(): TemplateResponse {
		$appKeys = $this->config->getAppKeys($this->appName);

		foreach ($this->defaultSettings as $key => $value) {
			$parameters["${key}_default"] = $value;
		}
		foreach ($appKeys as $key) {
			$parameters[$key] = $this->config->getAppValue($this->appName, $key);
		}

		return new TemplateResponse($this->appName, 'settings-admin', $parameters);
	}

	public function getSection(): string {
		return 'additional';
	}

	public function getPriority(): int {
		return 50;
	}
}
